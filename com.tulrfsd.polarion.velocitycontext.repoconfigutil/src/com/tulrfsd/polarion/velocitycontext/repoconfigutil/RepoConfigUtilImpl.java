package com.tulrfsd.polarion.velocitycontext.repoconfigutil;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import java.util.Properties;
import org.jetbrains.annotations.NotNull;
import com.polarion.alm.projects.IProjectService;
import com.polarion.alm.projects.model.IProject;
import com.polarion.platform.core.PlatformContext;
import com.polarion.platform.repository.config.IRepositoryConfigService;
import com.polarion.subterra.base.data.identification.IContextId;

public class RepoConfigUtilImpl implements RepoConfigUtil {

  @NotNull
  private final IRepositoryConfigService repositoryConfigService;
  private static IProjectService projectService = PlatformContext.getPlatform().lookupService(IProjectService.class);

  public RepoConfigUtilImpl(@NotNull IRepositoryConfigService repositoryConfigService) {
    this.repositoryConfigService = repositoryConfigService;
  }

  @Override
  public IRepositoryConfigService getRepositoryConfigService() {
    return repositoryConfigService;
  }

  @Override
  public Properties getProperties(IContextId contextId) throws IOException {
    return getProperties(contextId, "");
  }

  @Override
  public Properties getProperties(String projectId) throws IOException {
    return getProperties(projectId, "");
  }

  @Override
  public Properties getPropertiesForContextId(IContextId contextId, boolean inheritanceActive) {
    try {
      return getProperties(contextId);
    } catch (Exception e) {
      e.printStackTrace();
      return new Properties();
    }
  }

  @Override
  public Properties getProperties(IContextId contextId, String startString) throws IOException {
    Properties properties = new Properties();
    String rawProperties = (String) this.getRepositoryConfigService().getReadConfiguration("com.polarion.context.properties", contextId).getData();
    properties.load(new StringReader(rawProperties));
    if (startString == null || startString.isBlank()) {
      return properties;
    }
    Properties filteredProperties = new Properties();
    for (Map.Entry<Object, Object> entry : properties.entrySet()) {
      if (entry.getKey().toString().startsWith(startString)) {
        filteredProperties.put(entry.getKey().toString().replaceFirst(startString, ""), entry.getValue());
      }
    }
    return filteredProperties;
  }

  @Override
  public Properties getProperties(String projectId, String startString) throws IOException {
    IProject project = projectService.getProject(projectId);

    if (project.isUnresolvable()) {
      return new Properties();
    }
    return getProperties(project.getContextId(), startString);
  }

}
