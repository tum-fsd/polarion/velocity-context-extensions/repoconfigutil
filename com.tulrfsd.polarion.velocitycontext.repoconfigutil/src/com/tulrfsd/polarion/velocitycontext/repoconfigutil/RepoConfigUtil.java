package com.tulrfsd.polarion.velocitycontext.repoconfigutil;

import java.io.IOException;
import java.util.Properties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.polarion.platform.repository.config.IRepositoryConfigService;
import com.polarion.subterra.base.data.identification.IContextId;

/**
 * 
 * A helper to access the repository config service and the configuration properties as defined in
 * the admin panel.
 * 
 * @author Kevin Schmiechen (Technical University of Munich - Institute of Flight System Dynamics)
 */
public interface RepoConfigUtil {

  /**
   * Returns the repository config service. In rich page or wiki velocity context, it can be used by
   * following syntax:
   * 
   * <code>$repoConfigUtil.getRepositoryConfigService()</code>
   * 
   * @return the repository config service
   */
  @NotNull
  IRepositoryConfigService getRepositoryConfigService();

  /**
   * Returns the configuration properties for the provided context (e.g. project or global).
   * Properties with the string value <code>null</code> are ignored. By specifying a local property
   * with the value null, the value from the global property is effectively ignored. In rich page or
   * wiki velocity context, it can be used by following syntax:
   * 
   * <code>$repoConfigUtil.getPropertiesForContextId(IContextId contextId)</code>
   * 
   * @param contextId The context ID for which the properties are returned.
   * @return the properties object for the specified context ID
   * @throws IOException if there is an error reading the raw properties
   * @since 1.1.0
   */
  @NotNull
  Properties getProperties(@NotNull IContextId contextId) throws IOException;

  /**
   * Returns the configuration properties for the provided context (e.g. project or global) that
   * start with the defined startString. Properties with the string value <code>null</code> are
   * ignored. By specifying a local property with the value null, the value from the global property
   * is effectively ignored. In rich page or wiki velocity context, it can be used by following
   * syntax:
   * 
   * <code>$repoConfigUtil.getPropertiesForContextId(IContextId contextId, String startString)</code>
   * 
   * @param contextId The context ID for which the properties are returned.
   * @param startString Only include property entries where the key starts with startString. The
   *        startString is stripped from the keys of the entries. startString can be an empty
   *        string.
   * @return the filtered properties object for the specified context ID
   * @throws IOException if there is an error reading the raw properties
   * @since 1.2.0
   */
  @NotNull
  Properties getProperties(@NotNull IContextId contextId, @Nullable String startString) throws IOException;

  /**
   * Returns the configuration properties for the provided project ID. Properties with the string
   * value <code>null</code> are ignored. By specifying a local property with the value null, the
   * value from the global property is effectively ignored. In rich page or wiki velocity context,
   * it can be used by following syntax:
   * 
   * <code>$repoConfigUtil.getProperties(String projectId)</code>
   * 
   * @param projectId The ID of the project for which the properties are returned.
   * @return the properties object for the specified project ID or an empty object if the project is
   *         unresolvable
   * @throws IOException if there is an error reading the raw properties
   * @since 1.1.0
   */
  @NotNull
  Properties getProperties(@NotNull String projectId) throws IOException;

  /**
   * Returns the configuration properties for the provided project ID that start with the defined
   * startString. Properties with the string value <code>null</code> are ignored. By specifying a
   * local property with the value null, the value from the global property is effectively ignored.
   * In rich page or wiki velocity context, it can be used by following syntax:
   * 
   * <code>$repoConfigUtil.getProperties(String projectId)</code>
   * 
   * @param projectId The ID of the project for which the properties are returned.
   * @param startString Only include property entries where the key starts with startString. The
   *        startString is stripped from the keys of the entries. startString can be an empty or null.
   * 
   * @return the filtered properties object for the specified project ID or an empty object if the
   *         project is unresolvable
   * @throws IOException if there is an error reading the raw properties
   * @since 1.2.0
   */
  @NotNull
  Properties getProperties(@NotNull String projectId, @Nullable String startString) throws IOException;

  /**
   * Returns the configuration properties for the provided context (e.g. project or global).
   * Properties with the string value <code>null</code> are ignored. By specifying a local property
   * with the value null, the value from the global property is effectively ignored. In rich page or
   * wiki velocity context, it can be used by following syntax:
   * 
   * <code>$repoConfigUtil.getProperties(IContextId contextId)</code>
   * 
   * @param contextId The context ID for which the properties are returned.
   * @param inheritanceActive Activates or deactivates property inheritance from global level.
   * @return the properties object for the specified context ID
   * @deprecated As of 1.1.0 since disabling the inheritance has actually never worked (boolean
   *             parameter is non-functional in this method). Use {@link #getProperties(IContextId)}
   *             instead.
   */
  @Deprecated(since = "1.1.0", forRemoval = false)
  @NotNull
  Properties getPropertiesForContextId(IContextId contextId, boolean inheritanceActive);
}
