# RepoConfigUtil Velocity Context Extension

The latest build is available in the [releases section](../../releases).

## Introduction

This extension makes the `com.polarion.platform.repository.config.IRepositoryConfigService` available to the Velocity context.

Furthermore, it provides the configuration properties as an `java.util.Properties` object for a specified context or project ID.

See the JavaDoc in the releases for further details.

## License

Download and use of the extension is free. By downloading the extension, you accept the license terms.

See [NOTICE.md](NOTICE.md) for licensing details.


## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for contribution guidelines.


## Download & Installation

### Installation / Update:

1. Go to the [releases section](../../releases) of this repository and download the latest version.

1. Stop your Polarion server.

1. If it exists, delete the folder `com.tulrfsd.polarion.velocitycontext.repoconfigutil` in the `extensions` folder of your Polarion server.

1. Copy the root folder `com.tulrfsd.polarion.velocitycontext.repoconfigutil` of the release in the `extensions` folder of your Polarion server.

1. Delete the folder `../data/workspace/.config` from your Polarion installation.

1. Start your Polarion server.

### Uninstall:

1. Stop your Polarion server.

1. Delete the folder `com.tulrfsd.polarion.velocitycontext.repoconfigutil` in the `extensions` folder of your Polarion server.

1. Delete the folder `../data/workspace/.config` from your Polarion installation.

1. Start your Polarion server.

### Installation Requirements:

- Requires Polarion version 3.20.1 or later
- Tested with Polarion version 3.23.4